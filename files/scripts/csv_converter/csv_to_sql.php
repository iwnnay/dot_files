<?php

$data = [];
$options = [];
$values = [];
$arguments = $argv;
$configName = arg(1, 'alert');
$partner = 'suncoast';
$fileName = arg(2, '/convert_this.csv');
$outputFileName = arg(3, '/converted.sql');

$csv = fopen(__DIR__.$fileName, 'r');
$headers = fgetcsv($csv);

$optionsQuery = "INSERT INTO default_site_config_option (`key`, `name`, `data_type`, `added_by`, `added_on`, `modified_on`) VALUES ";
$valuesQuery = "INSERT INTO default_site_config_value (`default_site_config_option_id`, `value`) ";

while($row = fgetcsv($csv)) {
    $data[] = array_combine($headers, $row);
}

$converters = [];
$converts['activityTypeAlert'] = function($d) {
    $key = "activity_type_alert_config.${d['activity_type_id']}.${d['alert_name']}";
    add("$key.config_group_id", 1);
    add("$key.params", $d['params']);
};

$converters['activityTemplateAttribute'] = function($d) use ($configName, $partner) {
    $key = "$configName.$partner.{$d['activity_type_id']}.{$d['attribute']}.{$d['usage']}.{$d['view_cast']}";
    add("$key", $d['source']);
};

$converters['defaultConfigOptions'] = function($d) use ($configName, $partner) {
    add($d['key'], $d['value'], $d['data_type']);
};

$converters['alert'] = function($d) use ($configName, $partner) {
    $dataTypes = [
        'name' => 'text',
        'class' => 'text',
        'params'    => 'text',
        'exportPrevented'   => 'boolean',
        'triggerString'   => 'text',
        'type'      => 'text',
        'priority'  => 'integer',
        'scorecardThresholds'   => 'text',
        'isExportPreventedConfigurable' => 'boolean',
        'isExportPrevented' => 'boolean',
        'includeInScorecard'    => 'boolean',
        'includedAlways' => 'boolean',
        'externalNotificationType'  => 'text',
        'allowOverride' => 'boolean',
        'acknowledgeReasonRequired' => 'boolean'
    ];
    foreach ($d as $key => $value) {
        if ($key == 'key') { continue; }

        $alert = "$configName.{$d['key']}";
        add("$alert.$key", $value, $dataTypes[$key]);
    }
};

fclose($csv);

function add($key, $value, $dataType = 'text')
{
    global $options, $values, $valuesQuery;

    $value = is_null($value) ? 'NULL' : "'$value'";

    $options[] = " ('$key', '$key', '$dataType', 0, UNIX_TIMESTAMP(), UNIX_TIMESTAMP())";
    $values[] = "$valuesQuery select default_site_config_option_id, $value from default_site_config_option where `key` = '$key';\n";
}


foreach($data as $d) {
    $converters[$configName]($d);
}


if (count($options) === count($values)) {
    $sql = fopen(__DIR__.$outputFileName, 'w');

    $optionSQL = join($options, ",\n");
    fwrite($sql, "-- KEYS\n\n");
    fwrite($sql, "$optionsQuery \n$optionSQL;");

    fwrite($sql, "\n\n-- VALUES\n\n");
    foreach($values as $value) {
        fwrite($sql, $value);
    }

} else {
    echo "COUNTS DIDN'T MATCH";
}

function arg($get, $defaultTo)
{
    global $arguments;
    if(isset($arguments[$get])) {
        return $arguments[$get];
    } else if (isset($defaultTo)) {
        $arguments[$get] = $defaultTo;
        return $defaultTo;
    } else {
        return null;
    }
}
