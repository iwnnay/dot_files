# .bashrc
source ~/.git-completion.bash

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# to get the right color support
if [ -e /usr/share/terminfo/x/xterm-256color ]; then
    export TERM='xterm-256color'
else
    export TERM='xterm-color'
fi

alias wi5="cd /var/www/html/wi5hlist"
