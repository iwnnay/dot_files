"No Vi compatibility
set nocompatible

" Pathogen
"=======================================================
filetype off
call pathogen#helptags()
call pathogen#runtime_append_all_bundles()
execute pathogen#infect()

" General
"=======================================================

"Enable filetype detection, load plugin & indent files by type
filetype plugin indent on

"Force 256 colors
set t_Co=256

"Colorscheme
colorscheme jellybeans

"Enable syntax highlighting
syntax on

"Set leader for mappings
let mapleader=","
let g:mapleader=","

set encoding=utf8

" User Interface
"=======================================================
"Set color column
set colorcolumn=81

" Highlight current line and column
set cursorline
set cursorcolumn

"No visualbell or beep
set novisualbell
set t_vb=

"Reload files when changed outside of Vim
set autoread

"Show at least N lines above/below cursor when scroling vertically
set scrolloff=10

"Show cursor coordinates
set ruler

"Height of the command line
set cmdheight=1

"Show line numbers
set number

"Show invisible characters (TextMate style)
set list
set listchars=eol:�,tab:�\ ,trail:�

"Highlight search matches
set hlsearch

"Search while typing
set incsearch

"Ignore case in search
set ignorecase

"Override ignorecase if search has upper case
set smartcase

"Show matching brackets
set showmatch

"Do not wrap lines
set nowrap

"Show filename"
set ls=2

"Folding settings
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=1

" Editing
"=======================================================
set backspace=eol,start,indent "Backspace over line breaks, start of insert, and autoindent
set smartindent
set autoindent
set expandtab
set smarttab
set tabstop=4
set softtabstop=4
set shiftwidth=4

" GUI
"=======================================================
if has('gui_running')
    set guioptions-=T
    set guifont=Droid\ Sans\ Mono\ 9
    colorscheme jellybeans
endif

" Auto Commands
"=======================================================
if has('autocmd')
    autocmd BufEnter,BufNew,BufRead * :syntax sync fromstart

    autocmd BufNew,BufRead Gemfile,Capfile,Rakefile,*.ru set filetype=ruby
    autocmd BufNew,BufRead *.md set filetype=markdown
    autocmd BufNew,BufRead *.json set filetype=javascript
    autocmd BufNew,BufRead *.coffee set filetype=coffee
    autocmd BufNew,BufRead *.mustache set filetype=html
    autocmd BufNew,BufRead *.php set filetype=php

    autocmd FileType make set ts=8 sts=8 sw=8 noexpandtab
    autocmd Filetype ruby,scss set ts=2 sts=2 sw=2 expandtab
    autocmd FileType yaml,html,css,coffee,php,javascript set ts=4 sts=4 sw=4 expandtab
endif
set backupdir=/tmp
set directory=/tmp

" Syntastic
let g:ctrlp_working_path_mode = 1
let g:syntastic_auto_loc_list=1
let g:syntastic_enable_signs=1
let g:syntastic_stl_format=' [Syntax %E{Err: ln %fe (%e total)}%B{, }%W{Warn: ln %fw (%w total)}]'
let g:syntastic_twig_checkers = ['twig/twiglint']
let g:syntastic_twig_twiglint_exe = 'php /Users/Jimhoff/scripts/twig-lint.phar'
let g:syntastic_twig_twiglint_exec = 'php'

set statusline=%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P
set statusline+=%#warning#%{SyntasticStatuslineFlag()}%*

map  a<%=   %>hhhi
map  a<%   %>hhhi
map <F2> : Flisttoggle <CR>

function YamlFormat()
  g/^$/d
  g/^'$/d
  %s/! //g
  %s/sql: '/sql: |\r      /
  %s/''/'/g
  %s/title: '$/title: ''/
  %s/format_string: '$/format_string: ''/
endfunction

function CleanUpFormatting()
    let l:winview = winsaveview()
    retab
    %s/ *$//
    call winrestview(l:winview)
endfunction

function EscapeStringJs()
    '<,'>s/\([^\\]$\)/\1\\/
endfunction

function ConvertPrototypes()
    %s/^proto.\(.*\) =/\1:/
    %s/^\};/},/
    %s/var proto = \(.*\);$/\1 = {/
    normal GoGarnish.extendClass(, GCore);hhhhhhhhha
endfunction

"autocmd BufWritePre * :call CleanUpFormatting()
