# .bashrc
export PATH=/usr/local/php5:$PATH:/usr/local/bin
source ~/.git-completion.bash
export EDITOR=/usr/bin/vim
export JAVA_HOME=/usr/java/jdk1.6.0_31/


# Red Aril
export JAVA_OPTS="-XX:MaxPermSize=192m -Dredaril.cluster=localhost"

case $- in *i*) . ~/.bashrc;; esac

##
# Your previous /Users/jimhoff/.bash_profile file was backed up as /Users/jimhoff/.bash_profile.macports-saved_2016-01-18_at_13:53:17
##

# MacPorts Installer addition on 2016-01-18_at_13:53:17: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:/projects/work/vagrant_auxiliary_scripts/mac:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.

